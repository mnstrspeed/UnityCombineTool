﻿Shader "Mobile/Combined Diffuse"
{
	Properties
	{
		[NoScaleOffset] _MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }

		Pass
		{
			Tags {"LightMode"="ForwardBase"}

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			#include "UnityLightingCommon.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0; // original UV coords
				float4 uvRect : TEXCOORD1; // atlas x, y, width, height
				float4 uvScaleOffset : TEXCOORD2; // scale, offset in original UV coords
				fixed4 color : COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 uvRect : TEXCOORD1;
				float4 vertex : SV_POSITION;
				fixed4 diff : COLOR0;
			};

			uniform sampler2D _MainTex;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);

				// Texture coords
				//float2 localUV = v.uvScaleOffset.xy * v.uv + v.uvScaleOffset.zw;
				//float2 atlasUV = v.uvRect.xy + frac(v.uv) * v.uvRect.zw;
				o.uv = v.uvScaleOffset.xy * v.uv + v.uvScaleOffset.zw;
				o.uvRect = v.uvRect;

				// Lighting
				half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                o.diff = nl * _LightColor0;
                // Add illumination from ambient or light probes 
				// ShadeSH9 function from UnityCG.cginc evaluates it, using world space normal
                o.diff.rgb += ShadeSH9(half4(worldNormal, 1.));
				// Apply to vertex color
				o.diff *= v.color;

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// Sample texture
				float2 atlasUV = i.uvRect.xy + frac(i.uv) * i.uvRect.zw;
				fixed4 col = tex2D(_MainTex, atlasUV);
				// Apply diffuse lighting
				col *= i.diff;

				return col;
			}

			ENDCG
		}
	}
}
