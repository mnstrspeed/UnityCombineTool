using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System;

public class TileableCombineTool : EditorWindow
{
    private List<Renderer> selection;
    private List<Material> excludedMaterials;
    private Dictionary<Material, int> textureFactors;
    private int textureAtlasResolution = 2048;
    private Transform[] rootBones = new Transform[0];
    private bool includeRig = true;
    private bool includeBlendshapes = true;
    private bool disableOriginals = true;
    private string combinedName = "";
    private Shader combinedShader;

    #region EditorWindow

    [MenuItem("Tools/Combine into tilable atlas")]
    public static void CreateWindow()
    {
        var window = EditorWindow.GetWindow<TileableCombineTool>();

        // Initialize with current selection
        window.selection = Selection.gameObjects
            .SelectMany(go => go.GetComponentsInChildren<Renderer>())
            .Where(r => r is MeshRenderer || r is SkinnedMeshRenderer)
            .ToList();
        window.excludedMaterials = new List<Material>();
        window.textureFactors = new Dictionary<Material, int>();
        window.combinedShader = Shader.Find("Mobile/Combined Diffuse");
        
        window.Show();
    }

    private void Awake()
    {
        this.textureFactors = new Dictionary<Material, int>();
    }

    private void OnGUI()
    {
        var renderers = selection.ToArray();
        var materials = renderers.SelectMany(r => r.sharedMaterials).Distinct();

        EditorGUILayout.LabelField("Objects", EditorStyles.boldLabel);
        EditorGUILayout.BeginVertical(GUI.skin.box);

        var deletedRenderers = new List<Renderer>();
        foreach (var renderer in renderers)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(renderer.name);

            // Delete button
            if (GUILayout.Button("-", GUILayout.Width(20f)))
            {
                deletedRenderers.Add(renderer);
            }

            EditorGUILayout.EndHorizontal();
        }
        foreach (Renderer renderer in deletedRenderers)
            this.selection.Remove(renderer);

        GUIStyle noteStyle = new GUIStyle(GUI.skin.label);
        noteStyle.fontStyle = FontStyle.Italic;
        noteStyle.normal.textColor = Color.gray;
        EditorGUILayout.LabelField("Drop here to add more objects", noteStyle);
        EditorGUILayout.EndVertical();

        // Drag drop
        Rect dropArea = GUILayoutUtility.GetLastRect();

        var e = Event.current;
        switch (e.type)
        {
            case EventType.DragUpdated:
            case EventType.DragPerform:
                if (!dropArea.Contains(e.mousePosition))
                    break;

                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                if (e.type == EventType.DragPerform)
                {
                    DragAndDrop.AcceptDrag();
                    e.Use();

                    this.selection.AddRange(DragAndDrop.objectReferences
                        .Where(o => o is GameObject)
                        .SelectMany(go => (go as GameObject).GetComponentsInChildren<Renderer>())
                        .Where(r => r is MeshRenderer || r is SkinnedMeshRenderer)
                        .Except(this.selection));
                }

                break;
        }

        bool hasRig = renderers.Any(r => r is SkinnedMeshRenderer);
        EditorGUI.BeginDisabledGroup(!hasRig);

        this.includeRig = EditorGUILayout.Toggle("Include rig", hasRig && this.includeRig);
        if (hasRig && this.includeRig)
        {
            EditorGUI.indentLevel += 1;
            this.rootBones = new Transform[] 
            {
                EditorGUILayout.ObjectField("Root bone", 
                    this.rootBones.Length > 0 ? this.rootBones[0] : null, typeof(Transform), true) as Transform
            };
            EditorGUI.indentLevel -= 1;
        }

        EditorGUI.EndDisabledGroup();

        bool hasBlendshapes = renderers.Any(r => r is SkinnedMeshRenderer && 
            (r as SkinnedMeshRenderer).sharedMesh.blendShapeCount > 0);

        EditorGUI.BeginDisabledGroup(!hasBlendshapes);
        this.includeBlendshapes = EditorGUILayout.Toggle("Include blendshapes", hasBlendshapes && this.includeBlendshapes);
        EditorGUI.EndDisabledGroup();

        // Show total vertex count
        int vertexCount = renderers
            .Select(r => r is SkinnedMeshRenderer ? (r as SkinnedMeshRenderer).sharedMesh : r.GetComponent<MeshFilter>().sharedMesh)
            .Sum(m => m.vertexCount);
        EditorGUILayout.HelpBox("Vertex count: " + vertexCount, MessageType.Info);

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Materials", EditorStyles.boldLabel);

        EditorGUI.indentLevel += 1;
        foreach (var material in materials)
        {
            EditorGUILayout.BeginHorizontal();

            bool wasExcluded = this.excludedMaterials.Contains(material);
            bool excluded = !EditorGUILayout.Toggle(!wasExcluded, GUILayout.Width(26f));
            if (!wasExcluded && excluded)
                this.excludedMaterials.Add(material);
            else if (wasExcluded && !excluded)
                this.excludedMaterials.Remove(material);
            
            EditorGUILayout.LabelField(material.name);

            bool hasTexture = material.HasProperty("_MainTex");
            EditorGUI.BeginDisabledGroup(!hasTexture || excluded);
            
            int currentFactor = this.textureFactors.ContainsKey(material) ? this.textureFactors[material] : 1;
            bool canDecrement = currentFactor > 1;
            EditorGUI.BeginDisabledGroup(!canDecrement);

            bool decrementButtonClicked = GUILayout.Button("-", GUILayout.Width(20f));
            EditorGUI.EndDisabledGroup();
            EditorGUI.indentLevel -= 1;
            this.textureFactors[material] = EditorGUILayout.IntField(
                hasTexture ? currentFactor : 0, GUILayout.Width(30f));
            EditorGUI.indentLevel += 1;
            bool incrementButtonClicked = GUILayout.Button("+", GUILayout.Width(20f));

            if (incrementButtonClicked)
                this.textureFactors[material] = currentFactor + 1;
            if (decrementButtonClicked)
                this.textureFactors[material] = currentFactor - 1;

            EditorGUI.EndDisabledGroup();
            EditorGUILayout.EndHorizontal();
        }
        EditorGUI.indentLevel -= 1;

        this.textureAtlasResolution = EditorGUILayout.IntPopup("Texture atlas resolution", this.textureAtlasResolution,
            new string[] { "256", "512", "1024", "2048", "4096", "8192" },
            new int[] { 256, 512, 1024, 2048, 4096, 8192 });

        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.FloatField("Atlas island padding", 0.5f);
        EditorGUILayout.ObjectField("Shader", this.combinedShader, typeof(Shader), false);
        EditorGUI.EndDisabledGroup();


        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Miscellaneous", EditorStyles.boldLabel);
        this.combinedName = EditorGUILayout.TextField("Combined name", this.combinedName);
        this.disableOriginals = EditorGUILayout.Toggle("Disable originals", this.disableOriginals);

        // Buttons
        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Create", GUILayout.MinHeight(36f)))
        {
            this.CombineIntoTilableAtlas();
        }
        EditorGUILayout.EndHorizontal();
    }

    #endregion
    
    private struct BlendShapeFrame
    {
        public int ShapeIndex;
        public string ShapeName;
        public float FrameWeight;
        public int VertexOffset;
        public Vector3[] DeltaVertices;
        public Vector3[] DeltaNormals;
        public Vector3[] DeltaTangents;
    }

    public void CombineIntoTilableAtlas()
    {
        var active = this.selection[0].transform;
        Debug.Log(active.name);

        var renderers = selection;

        bool includeRig = this.includeRig && this.rootBones != null &&
            renderers.Any(renderer => renderer is SkinnedMeshRenderer &&
            (renderer as SkinnedMeshRenderer).bones.Length > 0);
        bool includeBlendshapes = this.includeBlendshapes &&
            renderers.Any(renderer => renderer is SkinnedMeshRenderer &&
            (renderer as SkinnedMeshRenderer).sharedMesh.blendShapeCount > 0);

        if (string.IsNullOrEmpty(this.combinedName))
        {
            this.combinedName = string.Join("-", selection.Select(go => go.name).ToArray()) + " (combined)";
        }

        // Index all textures
        var textures = renderers
            .SelectMany(r => r.sharedMaterials)
            .Except(this.excludedMaterials)
            .Where(m => m.HasProperty("_MainTex"))
            .Select(m => m.GetTexture("_MainTex")).Distinct().ToList();

        var blankTexture = GenerateBlankTexture(Color.white);
        textures.Add(blankTexture);

        // Ensure textures are readable
        foreach (var texture in textures)
            EnsureTextureReadable(texture);

        var oversizedTextureSizes = new Dictionary<Texture, int>();
        foreach (var kv in this.textureFactors)
        {
            var tex = kv.Key.GetTexture("_MainTex");
            if (tex != null)
                oversizedTextureSizes[tex] = kv.Value;
        }

        // Create atlas texture layout
        int[] textureSizes = new int[textures.Count];
        for (int i = 0; i < textures.Count; i++)
        {
            textureSizes[i] = textures[i] != null && oversizedTextureSizes.ContainsKey(textures[i]) ?
                oversizedTextureSizes[textures[i]] : 0;
        }

        Texture2D atlasTexture;
        Rect[] atlasRects = PackTilingTextures(out atlasTexture, textures.ToArray(), textureSizes, this.textureAtlasResolution, 0.5f);
        atlasTexture.name = this.combinedName;

        // Create mesh data
        var vertices = new List<Vector3>();
        var normals = new List<Vector3>();
        var uvs = new List<Vector2>(); // = original uv
        var uv1s = new List<Vector4>(); // = atlas min uv (xy), atlas max uv (zw)
        var uv2s = new List<Vector4>(); // = _MainTex_ST
        var colors = new List<Color>();
        var tris = new List<int>();

        var bones = new List<Transform>(); // original transform -> boneIndex
        var bindposes = new List<Matrix4x4>();
        var boneWeights = new List<BoneWeight>();
        var blendshapeFrames = new List<BlendShapeFrame>();

        foreach (Renderer renderer in renderers)
        {
            Mesh m = renderer is MeshRenderer ?
                renderer.GetComponent<MeshFilter>().sharedMesh :
                (renderer as SkinnedMeshRenderer).sharedMesh;
            int vertexOffset = vertices.Count;
            int vertexCount = m.vertexCount;

            Mesh vertexSourceMesh = m;
            if (renderer is SkinnedMeshRenderer)
            {
                vertexSourceMesh = new Mesh();
                (renderer as SkinnedMeshRenderer).BakeMesh(vertexSourceMesh);
            }

            vertices.AddRange(vertexSourceMesh.vertices.Select(v => active.InverseTransformPoint(renderer.transform.TransformPoint(v))));
            normals.AddRange(vertexSourceMesh.normals.Select(n => active.InverseTransformDirection(renderer.transform.TransformDirection(n))));
            uvs.AddRange(m.uv);
            uv1s.AddRange(Enumerable.Repeat(new Vector4(0, 0, 1, 1), vertexCount)); // default values
            uv2s.AddRange(Enumerable.Repeat(new Vector4(1, 1, 0, 0), vertexCount)); // default values
            colors.AddRange(Enumerable.Repeat(Color.white, vertexCount)); // default color

            if (includeRig)
            {
                var skinnedMeshRenderer = renderer as SkinnedMeshRenderer;

                // Translate local -> shared bone indices
                var boneIndices = new int[skinnedMeshRenderer.bones.Length];
                for (int localBoneIndex = 0; localBoneIndex < skinnedMeshRenderer.bones.Length; localBoneIndex++)
                {
                    var bone = skinnedMeshRenderer.bones[localBoneIndex];
                    if (!bones.Contains(bone))
                    {
                        // Add new shared bone
                        bones.Add(bone);

                        // Compute new corresponding bindpose (needs to be updated due to new mesh local space)
                        // "bindpose is a matrix that transforms mesh's local space into the localspace of the bone"
                        var bindPose = bone.worldToLocalMatrix * active.localToWorldMatrix;

                        bindposes.Add(bindPose);
                    }
                    boneIndices[localBoneIndex] = bones.IndexOf(bone);
                }
                // Determine bone weights
                for (int i = 0; i < m.vertexCount; i++)
                {
                    BoneWeight localWeight = m.boneWeights[i];
                    BoneWeight weight = localWeight;
                    // Change indices from local to shared
                    weight.boneIndex0 = boneIndices[localWeight.boneIndex0];
                    weight.boneIndex1 = boneIndices[localWeight.boneIndex1];
                    weight.boneIndex2 = boneIndices[localWeight.boneIndex2];
                    weight.boneIndex3 = boneIndices[localWeight.boneIndex3];

                    boneWeights.Add(weight);
                }
            }

            if (includeBlendshapes)
            {
                for (int shapeIndex = 0; shapeIndex < m.blendShapeCount; shapeIndex++)
                {
                    string shapeName = m.GetBlendShapeName(shapeIndex);
                    int frameCount = m.GetBlendShapeFrameCount(shapeIndex);

                    for (int frameIndex = 0; frameIndex < frameCount; frameIndex++)
                    {
                        var frame = new BlendShapeFrame();
                        frame.ShapeIndex = shapeIndex;
                        frame.ShapeName = shapeName;
                        frame.FrameWeight = m.GetBlendShapeFrameWeight(shapeIndex, frameIndex);
                        frame.VertexOffset = vertexOffset;

                        // Retrieve frame deltas
                        Vector3[] deltaVertices = new Vector3[m.vertexCount];
                        Vector3[] deltaNormals = new Vector3[m.vertexCount];
                        Vector3[] deltaTangents = new Vector3[m.vertexCount];
                        m.GetBlendShapeFrameVertices(shapeIndex, frameIndex, deltaVertices, deltaNormals, deltaTangents);

                        for (int i = 0; i < deltaVertices.Length; i++)
                        {
                            // Transform into new local mesh space
                            deltaVertices[i] = active.InverseTransformVector(renderer.transform.TransformVector(deltaVertices[i]));
                            deltaNormals[i] = active.InverseTransformDirection(renderer.transform.TransformDirection(deltaNormals[i]));
                            deltaTangents[i] = active.InverseTransformDirection(renderer.transform.TransformDirection(deltaTangents[i]));
                        }

                        frame.DeltaVertices = deltaVertices;
                        frame.DeltaNormals = deltaNormals;
                        frame.DeltaTangents = deltaTangents;

                        blendshapeFrames.Add(frame);
                    }
                }
            }

            for (int submesh = 0; submesh < m.subMeshCount; submesh++)
            {
                var indices = m.GetIndices(submesh);
                var submeshMaterial = renderer.sharedMaterials[submesh];

                var atlasIndex = submeshMaterial.HasProperty("_MainTex") ?
                    textures.IndexOf(submeshMaterial.GetTexture("_MainTex")) : -1;
                // Default to blank texture
                if (atlasIndex < 0 && !this.excludedMaterials.Contains(submeshMaterial))
                {
                    atlasIndex = textures.IndexOf(blankTexture);
                }

                if (atlasIndex >= 0)
                {
                    var atlasRect = atlasRects[atlasIndex];
                    var atlasRectVector = new Vector4(
                        atlasRect.xMin, atlasRect.yMin,
                        atlasRect.width, atlasRect.height);
                    var textureSTVector = new Vector4(
                        submeshMaterial.mainTextureScale.x, submeshMaterial.mainTextureScale.y,
                        submeshMaterial.mainTextureOffset.x, submeshMaterial.mainTextureOffset.y);

                    for (int i = 0; i < indices.Length; i++)
                    {
                        // Overwrites (hopefully still) default values
                        uv1s[vertexOffset + indices[i]] = atlasRectVector;
                        uv2s[vertexOffset + indices[i]] = textureSTVector;

                        tris.Add(vertexOffset + indices[i]);
                    }
                }

                if (submeshMaterial.HasProperty("_Color"))
                {
                    for (int i = 0; i < indices.Length; i++)
                    {
                        colors[vertexOffset + indices[i]] = submeshMaterial.color;
                    }
                }
            }
        }

        // Create mesh from data
        Mesh combinedMesh = new Mesh();
        combinedMesh.name = this.combinedName;
        combinedMesh.SetVertices(vertices);
        combinedMesh.SetNormals(normals);
        combinedMesh.SetColors(colors);
        combinedMesh.SetUVs(0, uvs);
        combinedMesh.SetUVs(1, uv1s);
        combinedMesh.SetUVs(2, uv2s);
        if (includeRig)
        {
            combinedMesh.bindposes = bindposes.ToArray();
            combinedMesh.boneWeights = boneWeights.ToArray();
        }
        if (includeBlendshapes)
        {
            foreach (var frame in blendshapeFrames)
            {
                // Pad deltas with zeros to match combinedMesh.vertexCount 
                Vector3[] combinedDeltaVertices = new Vector3[combinedMesh.vertexCount];
                Vector3[] combinedDeltaNormals = new Vector3[combinedMesh.vertexCount];
                Vector3[] combinedDeltaTangents = new Vector3[combinedMesh.vertexCount];
                Array.Copy(frame.DeltaVertices, 0, combinedDeltaVertices, frame.VertexOffset, frame.DeltaVertices.Length);
                Array.Copy(frame.DeltaNormals, 0, combinedDeltaNormals, frame.VertexOffset, frame.DeltaNormals.Length);
                Array.Copy(frame.DeltaTangents, 0, combinedDeltaTangents, frame.VertexOffset, frame.DeltaTangents.Length);

                combinedMesh.AddBlendShapeFrame(frame.ShapeName, frame.FrameWeight,
                    combinedDeltaVertices, combinedDeltaNormals, combinedDeltaTangents);
            }
        }
        combinedMesh.SetIndices(tris.ToArray(), MeshTopology.Triangles, 0, true);
        combinedMesh.RecalculateBounds();

        Material combinedMaterial = new Material(this.combinedShader);
        combinedMaterial.mainTexture = atlasTexture;
        combinedMaterial.name = this.combinedName;

        // Create combined GameObject
        GameObject combinedGO = new GameObject(this.combinedName);
        combinedGO.transform.SetParent(active.parent, false);
        combinedGO.transform.localPosition = active.transform.localPosition;
        combinedGO.transform.localRotation = active.transform.localRotation;
        combinedGO.transform.localScale = active.transform.localScale;

        if (includeRig)
        {
            var combinedSkinnedMeshRenderer = combinedGO.AddComponent<SkinnedMeshRenderer>();
            combinedSkinnedMeshRenderer.sharedMaterial = combinedMaterial;
            combinedSkinnedMeshRenderer.sharedMesh = combinedMesh;

            // Copy over bone hierarchy
            //combinedSkinnedMeshRenderer.bones = bones.ToArray();
            combinedSkinnedMeshRenderer.bones = CreateBoneHierarchy(combinedGO.transform, active.transform, this.rootBones.ToList(), bones);
        }
        else
        {
            combinedGO.AddComponent<MeshFilter>().sharedMesh = combinedMesh;
            combinedGO.AddComponent<MeshRenderer>().sharedMaterial = combinedMaterial;
        }

        Undo.IncrementCurrentGroup();
        Undo.RegisterCreatedObjectUndo(combinedGO, "Created " + combinedGO.name);

        if (this.disableOriginals)
        {
            Undo.RecordObjects(this.selection.ToArray(), "Disabled existing objects after combining");

            foreach (var original in this.selection)
                original.gameObject.SetActive(false);
        }

        // TODO: save Material/Texture/Mesh?
        string path = EditorUtility.SaveFilePanelInProject("Save assets",
            this.combinedName, "prefab", "");

        if (!string.IsNullOrEmpty(path))
        {
            if (AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)))
            {
                AssetDatabase.DeleteAsset(path);
            }

            //var go = PrefabUtility.CreatePrefab(path, combinedGO, ReplacePrefabOptions.ConnectToPrefab);
            var prefab = PrefabUtility.CreateEmptyPrefab(path);

            //AssetDatabase.CreateAsset(combinedMesh, path);
            AssetDatabase.AddObjectToAsset(combinedMesh, prefab);
            AssetDatabase.AddObjectToAsset(atlasTexture, prefab);
            AssetDatabase.AddObjectToAsset(combinedMaterial, prefab);

            // "Reimport the asset after adding an object.
            // Otherwise the change only shows up when saving the project"
            AssetDatabase.ImportAsset(path);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(combinedMesh));
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(atlasTexture));
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(combinedMaterial));

            AssetDatabase.Refresh();
            PrefabUtility.ReplacePrefab(combinedGO, prefab, ReplacePrefabOptions.ConnectToPrefab);
            AssetDatabase.SaveAssets();
        }

        DestroyImmediate(blankTexture);
    }

    private static Texture2D GenerateBlankTexture(Color color)
    {
        Texture2D texture = new Texture2D(2, 2, TextureFormat.RGBA32, false);
        texture.hideFlags = HideFlags.HideAndDontSave;

        var colors = Enumerable.Repeat(color, texture.width * texture.height);
        texture.SetPixels(colors.ToArray());
        texture.Apply();

        return texture;
    }

    private static void EnsureTextureReadable(Texture texture)
    {
        string assetPath = AssetDatabase.GetAssetPath(texture);
        var tImporter = AssetImporter.GetAtPath(assetPath) as TextureImporter;
        if (tImporter != null && !tImporter.isReadable)
        {
            tImporter.isReadable = true;

            AssetDatabase.ImportAsset(assetPath);
            AssetDatabase.Refresh();
        }
    }

    private static Rect[] CreateTextureAtlasLayout(int[] textureSizes, int atlasSize)
    {
        Debug.Log(textureSizes.Length);
        int maxTextureSize = textureSizes.Max();

        Vector2[] sizes = new Vector2[textureSizes.Length];
        for (int i = 0; i < textureSizes.Length; i++)
        {
            // 0 = smallest = atlasSize / 16
            // 1            = atlasSize / 8
            // 2            = atlasSize / 4
            // 3            = atlasSize / 2
            // 4 = largest  = atlasSize / 1

            int size = atlasSize / (1 << (maxTextureSize - textureSizes[i]));
            sizes[i] = size * Vector2.one;
        }

        List<Rect> results = new List<Rect>();
        while (!Texture2D.GenerateAtlas(sizes, 0, atlasSize, results))
        {
            // Keep halving sizes until they fits
            for (int i = 0; i < sizes.Length; i++)
            {
                sizes[i] *= 0.5f;
            }
        }

        // Normalize texture coords
        for (int i = 0; i < results.Count; i++)
        {
            results[i] = new Rect(results[i].position / atlasSize, results[i].size / atlasSize);
        }

        return results.ToArray();
    }

    private static Rect[] PackTilingTextures(out Texture2D atlas, Texture[] textures, int[] textureSizes, int maximumAtlasSize, float padding)
    {
        Rect[] atlasRects = CreateTextureAtlasLayout(textureSizes, maximumAtlasSize);

        atlas = new Texture2D(maximumAtlasSize, maximumAtlasSize, TextureFormat.RGBA32, true);
        RenderTexture temp = RenderTexture.GetTemporary(atlas.width, atlas.height, 0);
        RenderTexture.active = temp;

        GL.PushMatrix();
        GL.LoadOrtho();

        Material mat = new Material(Shader.Find("Hidden/UnityCombineTool/TexturePackingBlit"));

        for (int i = 0; i < textures.Length; i++)
        {
            Rect rect = atlasRects[i];

            mat.mainTexture = textures[i];
            mat.mainTextureScale = Vector2.one * (1f + 2f * padding);
            mat.mainTextureOffset = -1f * Vector2.one * padding;

            mat.SetPass(0);

            GL.Begin(GL.QUADS);
            GL.TexCoord2(0f, 0f);
            GL.Vertex3(rect.x, rect.y, 0f);
            GL.TexCoord2(0f, 1f);
            GL.Vertex3(rect.x, rect.y + rect.height, 0f);
            GL.TexCoord2(1f, 1f);
            GL.Vertex3(rect.x + rect.width, rect.y + rect.height, 0f);
            GL.TexCoord2(1f, 0f);
            GL.Vertex3(rect.x + rect.width, rect.y, 0f);
            GL.End();
        }

        GL.PopMatrix();

        // Copy back to atlas
        atlas.ReadPixels(new Rect(0, 0, atlas.width, atlas.height), 0, 0, true);
        atlas.Apply();

        RenderTexture.active = null;
        DestroyImmediate(temp);

        Rect[] atlasRectsWithoutPadding = atlasRects.Select(rect => new Rect(
            rect.position + padding / (1f + 2f * padding) * rect.size,
            rect.size * 1f / (1f + 2f * padding))).ToArray();

        return atlasRectsWithoutPadding;
    }

    private static Transform[] CreateBoneHierarchy(Transform parent, Transform originalParent, List<Transform> roots, List<Transform> originalBones)
    {
        //var roots = originalBones.Where(b => !originalBones.Any(other => b != other && b.IsChildOf(other))).ToList();
        //var roots = new List<Transform>() { originalParent.Find("Armature") };
        var copiedRoots = roots.Select(root =>
        {
            var copiedRoot = Instantiate(root, parent);
            copiedRoot.name = root.name; // must remove "(Clone)" from name, otherwise animations don't work
            return copiedRoot;
        }).ToList();

        var newBones = new Transform[originalBones.Count];
        for (int i = 0; i < originalBones.Count; i++)
        {
            var bone = originalBones[i];
            var rootIndex = roots.FindIndex(root => bone.IsChildOf(root));
            var copiedRoot = copiedRoots[rootIndex];

            if (roots.Contains(bone))
            {
                // Bone is a root
                newBones[i] = copiedRoot;
            }
            else
            {
                // Bone is a child
                newBones[i] = copiedRoot.Find(GetPathRelative(bone, roots[rootIndex]));
            }
        }

        return newBones;
    }

    private static string GetPathRelative(Transform child, Transform root)
    {
        // Path excluding root
        string path = "";

        Transform current = child;
        while (current != root && current.root != current)
        {
            string separator = path.Length > 0 ? "/" : "";
            path = current.name + separator + path;

            current = current.parent;
        }

        return path;
    }

    [MenuItem("Tools/Test submesh materials")]
    public static void TestSubMeshMaterials()
    {
        var materials = Selection.activeGameObject.GetComponent<MeshRenderer>().sharedMaterials;
        var mesh = Selection.activeGameObject.GetComponent<MeshFilter>().sharedMesh;

        string result = string.Format("{0} (material count = {1}, submesh count = {2})",
            materials.Length == mesh.subMeshCount ? "Match" : "Failed",
            materials.Length, mesh.subMeshCount);

        EditorUtility.DisplayDialog("Submesh Material Test", result, "OK");
    }
}